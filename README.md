## Avant de lancer le code penser à activer le venv (source venv/bin/activate)
## Rendu ADM CO

### ReadMe


Ce TP ce divise en deux parties distinctes avec chacune un objectif différent.

Dans un premier temps, nous devions créer deux packages, un qui permet de lancer
des calculs simple, l'autre qui permet de mettre en place des tests capables de 
vérifier que chaques aspect des calculs fonctionnent (seulement pour des entiers
puis pas de division par 0).
Dans un second temps on a voulu "automatiser" ces tests grâce à l'intégration
continue.


#### Packages

On verifiera bien que toute les étapes de création d'un package ont été, comme 
les fichiers init.py ou encore que les chemins lors des importations ont été
bien respectés.
Enfin pour le bon fonctionnement de pytest on pensera à insérer un "_" dans le nom
du fichier test.

#### Intégration Continue

L'intégration continue permet de lancer les tests unitests préablement codés, sur
un environnement virtuel créé, et ce à chaque fois que cela est nécessaire, par 
exemple à chaque fois qu'une modificationest fait sur le repository. 
Elle nécessite deux fichiers, un fichier texte "requirements.txt" qui permet d'exposer 
les conditions préalables nécessaire au lancement du test. 
Un fichier .yml qui permet de définir les différentes opérations à effectuer ainsi que leur
ordre.

