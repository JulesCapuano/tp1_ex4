# -*- coding: utf-8 -*-
from SimpleCalculator.model.simple_calculator import SimpleCalculator
import unittest
import logging

class AddTest(unittest.TestCase):
    def setUp(self): #Initialisation et définition de la classe
        self.calculator = SimpleCalculator()

    def test_add(self):
        result = self.calculator.sum(3, 4)#On crée la variable result contenant le résultat de la fonction sum
        self.assertEqual(result, 7)#On vérifie que result prend bien la valeur 7
        logging.warning('I hope this is two integers')
        logging.info('Otherwile it will crash...') 

    def test_add_float(self):
        result = self.calculator.sum(8, 2.2)
        self.assertEqual(result, "Error sum take two ints")#En cas d'erreur on vérifie
        #que la fonction renvoi bien le même message ici "Error sum take two integers.


class SubtractTest(unittest.TestCase):
    def setUp(self):
        self.calculator = SimpleCalculator()

    def test_substract(self):
        result = self.calculator.substract(3, 4)
        self.assertEqual(result, -1)

    def test_subtract_float(self):
        result = self.calculator.substract(8, 3.6)
        self.assertEqual(result, "Error substract take two ints")
       


class MultiplyTest(unittest.TestCase):
    def setUp(self):
        self.calculator = SimpleCalculator()

    def test_multiply(self):
        result = self.calculator.multiply(3, 4)
        self.assertEqual(result, 12)

    def test_multiply_float(self):
        result = self.calculator.multiply(3, 4.3)
        self.assertEqual(result, "Error multiply take two ints")
      


class DivideTest(unittest.TestCase):
    def setUp(self):
        self.calculator = SimpleCalculator()

    def test_divide(self):
        result = self.calculator.divide(12, 3)
        self.assertEqual(result, 4)

    def test_divide_string(self):
        result = self.calculator.divide(12, "3")
        self.assertEqual(result, "Error divide take two ints")
 

    def test_divide_by_zero(self):
        with self.assertRaises(ZeroDivisionError):#On vérifie que l'erreur se déclenche bien"
            self.calculator.divide(10, 0)


if __name__ == "__main__":
    unittest.main()