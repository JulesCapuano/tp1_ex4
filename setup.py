import setuptools
from distutils.core import setup

setup(
    name='SimpleCalculator',
    version='0.1dev',
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    author="Jules Capuano",
    author_email="jules.capuano@cpe.fr",
    description="Calculator",
    package_dir={"": "SimpleCalculator/model"},
    packages=setuptools.find_namespace_packages(where="SimpleCalculator/model"))
