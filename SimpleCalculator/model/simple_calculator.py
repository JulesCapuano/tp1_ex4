# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""


class SimpleCalculator:

    @staticmethod
    def sum(nbr1, nbr2):
        if isinstance(nbr1, int) and isinstance(nbr2, int):
            return nbr1 + nbr2
        else:
            return "Error sum take two ints"

    @staticmethod
    def substract(nbr1, nbr2):
        if isinstance(nbr1, int) and isinstance(nbr2, int):
            return nbr1 - nbr2
        else:
            return "Error substract take two ints"

    @staticmethod
    def multiply(nbr1, nbr2):
        if isinstance(nbr1, int) and isinstance(nbr2, int):
            return nbr1 * nbr2
        else:
            return "Error multiply take two ints"

    @staticmethod
    def divide(nbr1, nbr2):
        if isinstance(nbr1, int) and isinstance(nbr2, int):
            return nbr1 / nbr2
        elif nbr2 == 0:
            raise ZeroDivisionError("Cannot divide by zero")
        else:
            return "Error divide take two ints"
